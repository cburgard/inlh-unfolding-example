### HELPERS
############

def get_unfolded_histogram(workspace,hist_template,parameter_template):
    unfolded = hist_template.Clone()
    unfolded.Reset()
    truth_xs = [workspace.var(parameter_template.format(i)) for i in range(1,unfolded.GetNbinsX()+1)]
    for i in range(1,unfolded.GetNbinsX()+1):
        if not truth_xs[i-1]:
            raise RuntimeError("failed to retrieve '"+parameter_template.format(i-1)+"'")
        unfolded.SetBinContent(i,truth_xs[i-1].getVal())
        unfolded.SetBinError(i,truth_xs[i-1].getError())
    return unfolded

_protected = []
def protect(smth):
    _protected.append(smth)
    return smth

def nbins(hist,includeUnderflow=False):
    """get the number of bins from any histogram"""
    return len(list(iterbins(hist,includeUnderflow)))
    
def iterbins(hist,includeUnderflow=False):
    """iterate over the number of bins of any histogram"""
    from ROOT import TH1, TH2, TH3
    for i in range(0,hist.GetNbinsX()+2):
        for j in range(0,hist.GetNbinsY()+2) if hist.InheritsFrom(TH2.Class()) or hist.InheritsFrom(TH3.Class()) else [0]:
            for k in range(0,hist.GetNbinsZ()+2) if hist.InheritsFrom(TH3.Class()) else [0]:
                b = hist.GetBin(i,j,k)
                if includeUnderflow:
                    yield b
                elif not hist.IsBinOverflow(b) and not hist.IsBinUnderflow(b):
                    yield b

def clone_histograms(mymap):
    return { name:hist.Clone() for name,hist in mymap }
                    
def cppyy_hist2d_cast(orighist):
    import ROOT
    import cppyy
    if isinstance(orighist, ROOT.TH2):
        hist = orighist
    else:
        hist = orighist.Clone()
        if not hist:
            raise TypeError("histogram {:s} is not of type TH2".format(hist.Getname()))
        else:
            print("cast {:s} to TH2 via Clone".format(orighist.GetName()))
    return hist
                    
def slice_histogram(hist2d, axis="x", includeUnderflow=False):
    retval = [ hist2d.ProjectionY("bin_"+str(i),i,i) if axis == "y" else hist2d.ProjectionX("bin_"+str(i),i,i) for i in range( 0 if includeUnderflow else 1 , hist2d.GetNbinsY()+1 if axis == "y" else hist2d.GetNbinsX()+1 ) ]
    for x in retval:
        x.SetDirectory(0)
    return retval

def project_histogram(hist2d, axis="x"):
    retval = hist2d.ProjectionY("integral") if axis == "y" else hist2d.ProjectionX("integral")
    retval.SetDirectory(0)
    return retval
    
def hist2JSON(h, includeUnderflow=False):
    """convert a histogram to the HS3 JSON representation (data-block only)"""
    return {
        "contents" : [ h.GetBinContent(i) for i in iterbins(h,includeUnderflow) ],
        "errors" : [ h.GetBinError(i) for i in iterbins(h,includeUnderflow) ],
    }

def axes2JSON(h,varnames,includeUnderflow = False):
    """convert the axes of a histogram to their respective HS§ JSON representation"""
    from ROOT import TH1, TH2, TH3
    axes = [{
        "name" : varnames[0],
        "min" : h.GetXaxis().GetXmin(),
        "max" : h.GetXaxis().GetXmax(),
        "nbins" : h.GetXaxis().GetNbins()
    }]    
    if h.InheritsFrom(TH2.Class()) or h.InheritsFrom(TH3.Class()):
        axes.append({
            "name" : varnames[1],
            "min" : h.GetYaxis().GetXmin(),
            "max" : h.GetYaxis().GetXmax(),
            "nbins" : h.GetYaxis().GetNbins()
        })
    if h.InheritsFrom(TH3.Class()):        
        axes.append({
            "name" : varnames[2],
            "min" : h.GetZaxis().GetXmin(),
            "max" : h.GetZaxis().GetXmax(),
            "nbins" : h.GetZaxis().GetNbins()
        })
    return axes

def collect_histograms(histograms,infilename):
    """collect all the histograms from a file and store them in a dictionary for easy access"""
    from ROOT import TFile
    if infilename.endswith(".root"):
        infile = TFile.Open(infilename,"READ")
        for k in infile.GetListOfKeys():
            hist = k.ReadObj().Clone()
            hist.SetDirectory(0)
            histograms[k.GetName()]  = hist
        infile.Close()
    else:
        raise RuntimeError("unknown file type: "+infilename)

### I/O
############
    
def read_config(configname):
    """read a JSOn config file and make it available in memory verbatim"""
    import json
    with open(configname,"rt") as infile:
        return json.load(infile)

def ensure_minimum(hist,val):
    for i in iterbins(hist):
        if hist.GetBinContent(i) < val:
            hist.SetBinContent(i,val)
    
def construct_slices(reco,migration,truth,all_histograms,scale_XS,scale_density,reco_axis="x"):
    slice_dict = { k:slice_histogram(cppyy_hist2d_cast(all_histograms[v]),axis=reco_axis,includeUnderflow=False) for k,v in migration.items() }
    num = 0
    for name in migration.keys():
        if name in reco.keys():
            reco_dist = all_histograms[reco[name]]
            remainder = reco_dist.Clone()
            remainder.SetName("fakes")
            do_fakes = True
        else:
            reco_dist = project_histogram(cppyy_hist2d_cast(all_histograms[migration[name]]), axis=reco_axis)
            do_fakes  = False
        if name in truth.keys():
            truth_dist = all_histograms[truth[name]]
        else:
            truth_dist = None
        for index,slice in enumerate(slice_dict[name]):
            bin_yield = slice.Integral()
            if bin_yield <= 0:
                slice.Reset()
                continue
            fiducial_yield = truth_dist.GetBinContent(index+1)
            if do_fakes: 
                remainder.Add(slice,-1)
            slice.Scale(1. if scale_XS else 1./fiducial_yield)
        if not scale_XS:
            remainder.Scale(1./remainder.Integral())
        if remainder:
            ensure_minimum(remainder,0.)
        slice_dict[name].append(remainder)
        num = len(slice_dict[name])
    return [ { migration[name]:slice_dict[name][i] for name in migration.keys() } for i in range(num) ]
    
def build_model(config,histograms,data_dict):
    """based on a config file and a set of histograms, build the HS3 JSON model of the likelihood"""
    model = {
        "metadata" : { "hs3_version" : "0.2" },
        "distributions" : [],
        "functions" : [],
        "domains" : [ { "name" : "default_domain", "type" : "product_domain", "axes" : []} ],
        "parameter_points" : [ { "name" : "default_values", "parameters" : []}, { "name" : "background_only", "parameters" : []} ],        
        "data" : [],
        "likelihoods" : [{"name":"simultaneous_likelihood","data":[],"distributions":[],"aux_distributions":[]}],
        "analyses" : [{"name":"unfolded_model","likelihood":"simultaneous_likelihood"}]
    }
    reg_terms = []
    settings = config["settings"]
    include_sys = settings.get("include_systematics",True)
    for channel_name,channel_config in config["channels"].items():
        channel = { "samples" : [] , "axes" : None, "type" : "histfactory_dist", "name":channel_name }
        model["distributions"].append(channel)
        for sample_name,sample_config in channel_config["samples"].items():
            if "unfolding" in sample_config.keys():
                poitype = sample_config["unfolding"]["poitype"]
                migration_config = sample_config["unfolding"]["migration"]
                truth_config = sample_config["unfolding"]["truth"]
                if poitype == "ss":
                    scale_XS = True
                elif poitype == "cs":
                    scale_XS = False
                nom_slices = construct_slices({"nominal":sample_config["data"]},{"nominal":migration_config["data"]},{"nominal":truth_config["data"]},histograms,scale_XS,True)
                samples = []
                pois = []
                parameters = model["parameter_points"][0]["parameters"]
                nullparameters = model["parameter_points"][1]["parameters"]                
                axes = model["domains"][0]["axes"]
                for idx,nom in enumerate(nom_slices):
                    label = nom[migration_config["data"]].GetName()
                    poiname = sample_config["unfolding"]["poi"]+"_"+label
                    if idx < histograms[truth_config["data"]].GetNbinsX()+1:
                        truth_xs = histograms[truth_config["data"]].GetBinContent(idx+1)
                    else:
                        truth_xs = 0
                    sample = {"data":hist2JSON(nom[migration_config["data"]]), "name":sample_name+"_"+label, "modifiers":[]}
                    if poitype == "ss":
                        sample["modifiers"].append({"name":poiname,"type":"normfactor"})
                        parameters.append({"name":poiname,"value":1.})
                        nullparameters.append({"name":poiname,"value":0.})                        
                        axes.append({"name":poiname,"min":-50,"max":50})
                    elif poitype == "cs":
                        sample["modifiers"].append({"name":poiname,"type":"normfactor"})
                        parameters.append({"name":poiname,"value":truth_xs})
                        nullparameters.append({"name":poiname,"value":0.})                                                
                        axes.append({"name":poiname,"min":-50*truth_xs,"max":50*truth_xs})
                    else:
                        raise RuntimeError("unknown poi type: "+poitype+" (must be one of ss,cs)")
                    pois.append(poiname)
                    samples.append(sample)
                    channel["samples"].append(sample)                                            
                    if not channel["axes"]:
                        axes = axes2JSON(nom[sample_config["data"]],channel_config["variables"])
                        channel["axes"] = axes
                        model["domains"][0]["axes"] += axes
                if include_sys and "modifiers" in migration_config.keys():
                    hi_slices  = construct_slices({ m["name"]:m["data"]["hi"] for m in sample_config["modifiers"]},{m["name"]:m["data"]["hi"] for m in migration_config["modifiers"]},{m["name"]:m["data"]["hi"] for m in truth_config["modifiers"]},histograms,scale_XS,True)
                    lo_slices  = construct_slices({ m["name"]:m["data"]["lo"] for m in sample_config["modifiers"]},{m["name"]:m["data"]["lo"] for m in migration_config["modifiers"]},{m["name"]:m["data"]["lo"] for m in truth_config["modifiers"]},histograms,scale_XS,True)
                    for sample,hi,lo in zip(samples,hi_slices,lo_slices):
                        for modifier in migration_config["modifiers"]:
                            m = {"type":"histosys","constraint":"Gauss","data":{"hi":hist2JSON(hi[modifier["data"]["hi"]]),"lo":hist2JSON(lo[modifier["data"]["lo"]])}}
                            if "name" in modifier.keys():
                                m["name"] = modifier["name"]
                            else:
                                m["name"] = modifier["data"]["hi"]
                            sample["modifiers"].append(m)
                if "regularize" in sample_config["unfolding"].keys():
                    regularization = sample_config["unfolding"]["regularize"]
                    if regularization == False:
                        pass
                    elif regularization["type"] == "tikhonov":
                        curvature = regularization["curvature"]
                        if curvature == "cs":
                            expression = [ "pow("+pois[i-1] +"-2*"+pois[i]+"+"+pois[i+1]+",2)" for i in range(1,len(pois)-2) ]
                        elif curvature == "ss":
                            nomvals = []
                            for i in range(0,len(pois)):
                                nomval = "nom_"+pois[i]
                                nomvals.append(nomval)
                                model["parameter_points"][0]["parameters"].append({"name":nomval,"const":True,"value":histograms[truth_config["data"]].GetBinContent(i+1)})
                            expression = [ "pow("+pois[i-1]+"/"+nomvals[i-1]  +"-2*"+pois[i] + "/" + nomvals[i] +"+"+pois[i+1]+"/"+nomvals[i+1]+",2)" for i in range(1,len(pois)-2) ]
                        else:
                            raise RuntimeError("unknown curvature: "+curvature)                            
                        tau_name = "tau_"+sample_name
                        model["domains"][0]["axes"].append({"name":tau_name,"min":1e-5,"max":1e9})
                        model["parameter_points"][0]["parameters"].append({"name":tau_name,"value":regularization["strength"],"const":True})
                        model["functions"].append({ "name":"tikhonov_differential_"+sample_name, "type":"generic_function", "expression" : "+".join(expression)})
                        reg_terms.append({"name":"tikhonov_distribution_"+sample_name,"type":"exponential_dist","c":tau_name,"x":"tikhonov_differential_"+sample_name})
                    else:
                        raise RuntimeError("unknown regularization: "+regularization)
            else:
                histogram = histograms[sample_config["data"]]
                sample = {"data":hist2JSON(histogram), "name":sample_name, "modifiers":[]}
                if include_sys and "modifiers" in sample_config.keys():
                    for modifier in sample_config["modifiers"]:
                        m = {"type":"histosys","constraint":"Gauss","data":{"hi":hist2JSON(histograms[modifier["data"]["hi"]]),"lo":hist2JSON(histograms[modifier["data"]["lo"]])}}
                        if "name" in modifier.keys():
                            m["name"] = modifier["name"]
                        else:
                            m["name"] = modifier["data"]["hi"]
                        sample["modifiers"].append(m)
                if not channel["axes"]:
                    axes = axes2JSON(histogram,channel_config["variables"])
                    channel["axes"] = axes
                    model["domains"][0]["axes"] += axes
                channel["samples"].append(sample)
        model["likelihoods"][0]["distributions"].append(channel_name)
    model["analyses"][0]["parameters_of_interest"] = pois
    for data_name,data_hist in data_dict.items():
        model["likelihoods"][0]["data"] = [data_name]
        data = hist2JSON(data_hist)
        data["axes"] = axes2JSON(data_hist,config["channels"][data_name]["variables"])
        data["type"] = "binned"
        data["name"] = data_name
        model["data"].append(data)
    for reg_term in reg_terms:
        model["likelihoods"][0]["aux_distributions"].append(reg_term["name"])
        model["distributions"].append(reg_term)        


    return model

def build_workspace(model):
    """convert an HS3 representation of a model into the corresponding ROOT workspace"""
    from ROOT import RooJSONFactoryWSTool, RooWorkspace
    workspace = RooWorkspace("workspace")
    tool = RooJSONFactoryWSTool(workspace)

    # this is a bit silly for now, we'll fix it later
    import json
    modelstring = json.dumps(model)
    tool.importJSONfromString(modelstring)

    for var in model["domains"][0]["axes"]:
        if "nbins" in var.keys():
            workspace.var(var["name"]).setBins(var["nbins"])
    
    return workspace

def write_workspace(workspace,outfilename):
    """write a workspace from memory into a file"""
    workspace.writeToFile(outfilename)

### STEERING
############
    
def main(args):
    """main steering function"""
    histograms = {}
    for infile in args.histograms:
        collect_histograms(histograms,infile)

    config = read_config(args.config)

    model = build_model(config,histograms)

    if args.outmodelname:
        with open(args.outmodelname,"wt") as outfile:
            import json
            json.dump(model,outfile)

    workspace = build_workspace(model)

#    unfolded = unfold(config,workspace)
    
    write_workspace(workspace,args.outfilename)
    
if __name__ == "__main__":
    from argparse import ArgumentParser
    parser = ArgumentParser(description="build an in-likelihood unfolding")
    parser.add_argument("--histograms",nargs="+",help="files with histograms",required=True)
    parser.add_argument("--config",help="config file",required=True)
    parser.add_argument("--output","-o",dest="outfilename",help="output file",required=True)
    parser.add_argument("--output-model","-om",dest="outmodelname",help="output model")            
    args = parser.parse_args()
    main(args)
